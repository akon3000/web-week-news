const routes = require('next-routes')

module.exports = routes()
// Name, Pattern, ActualPage
// ====  ======= ===========
.add('home', '/', '/client/home')
.add('login', '/login', '/client/login')
.add('news', '/news/:id', '/client/news')
.add('adminHome', '/admin', '/admin/home')
.add('adminCreateNews', '/admin/create', '/admin/create')
.add('adminEditNews', '/admin/edit/:id', '/admin/edit')