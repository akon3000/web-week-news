import Router from 'next/router'

export default (statusCode) => {
  Router.replace({
    pathname: '/_error',
    query: {
      statusCode
    }
  })
}