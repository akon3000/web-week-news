import { string, bool } from 'prop-types'
import { TextArea } from 'semantic-ui-react'
import styled, { css } from 'styled-components'
import { neutralColors, utilityColors } from '../../utils/css'

const LabelField = styled('label')`
  display: block;
  margin-bottom: 10px;
  color: ${neutralColors.solidGrey};
`

const LabelError = styled('label')`
  display: block;
  margin: 10px 0;
  color: ${utilityColors.danger};
`

const Wrapper = styled('div')`
  ${({ hasError }) => hasError && css`
    .textarea-field {
      border: solid 1px ${utilityColors.danger} !important;
      &:focus {
        border: solid 1px ${utilityColors.danger} !important;
      }
    }
  `}
`

const FieldArea = ({
  label,
  fluid,
  error,
  className,
  ...rest
}) => (
  <Wrapper hasError={error}>
    {label && <LabelField>{label}</LabelField>}
    <TextArea
      className={`textarea-field ${className}`}
      {...rest}
    />
    {error && <LabelError>{error}</LabelError>}
  </Wrapper>
)

FieldArea.propTypes = {
  label: string,
  error: string,
  className: string
}

FieldArea.defaultProps = {
  className: ''
}

export default FieldArea