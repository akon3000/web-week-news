import { useState } from 'react'
import Router from 'next/router'
import styled, { css } from 'styled-components'
import { string, object } from 'prop-types'
import { Form, Divider, Button } from 'semantic-ui-react'
import { neutralColors } from '../../utils/css'
import { getCookie } from '../../utils/cookie'
import baseApiUrl from '../../utils/baseApiUrl'
import requestApi, { withAuth } from '../../utils/requestApi'
import errorRequest from '../../utils/errorRequest'
import Field from '../../components/Field'
import FieldArea from '../../components/FieldArea'

const FieldBox = styled('div')`
  width: 100%;
  ${({ fluid }) => !fluid && css`
    max-width: 500px;
  `}
  margin-bottom: 15px;
`

const FieldBoxImage = styled('div')`
  width: 100%;
  max-width: 200px;
  & > img {
    width: 100%;
    height: auto;
  }
`

const FieldBoxButton = styled('div')`
  text-align: right;
`

const Wrapper = styled('div')`
  padding: 20px;
  background-color: ${neutralColors.white};

  .btn-form {
    width: 120px;
  }
  .divider-form {
    margin: 20px 0;
  }
`

const CreateEditNews = ({
  user,
  type,
  news
}) => {
  const initForm = {
    title: '',
    cover_image: '',
    long_description: '',
    short_description: ''
  }
  const initErrorForm = {
    title: null,
    short_description: null
  }
  const titlePage = {
    create: 'Create news',
    edit: 'Edit news'
  }[type]

  const [isLoading, setIsLoading] = useState(false)
  const [form, setForm] = useState(news || initForm)
  const [formError, setFormError] = useState(initErrorForm)

  const handleCreateNews = async () => {
    setIsLoading(true)
    try {
      const token = getCookie('authToken')

      const { data: response } = await requestApi.post(`${baseApiUrl()}/news/create`, { ...form, create_by: user.email }, withAuth(token))

      setIsLoading(false)
      alert(response.message)

      if (response.status) {
        Router.push({ pathname: '/admin', asPath: '/admin/home' })
      }

    } catch (err) {
      setIsLoading(false)
      if (err.response) {
        errorRequest(err.response.status)
      }
    }
  }

  const handleEditNews = async () => {
    setIsLoading(true)
    try {
      const token = getCookie('authToken')

      const { data: response } = await requestApi.put(`${baseApiUrl()}/news/edit/${news.id}`, form, withAuth(token))

      setIsLoading(false)
      alert(response.message)

      if (response.status) {
        Router.push({ pathname: '/admin', asPath: '/admin/home' })
      }

    } catch (err) {
      setIsLoading(false)
      if (err.response) {
        errorRequest(err.response.status)
      }
    }
  }

  const handleSubmit = async e => {
    e.preventDefault()
    
    const error = handleValidateForm()

    if (error.title || error.short_description) {
      setFormError(error)
      return
    }

    if (type === 'create') {
      handleCreateNews()
    } else if (type === 'edit') {
      handleEditNews()
    }
   
  }

  const handleValidateForm = () => {
    const error = {}

    if (!form.title) {
      error.title = 'This field is required'
    }

    if (form.short_description.length > 250) {
      error.short_description = `Field can't be more than 250 characters long`
    }

    return error
  }

  return (
    <Wrapper>
      <h2>{titlePage}</h2>
      <Divider />
      <Form onSubmit={handleSubmit}>
        <FieldBox>
          <Field
            label='News title'
            value={form.title}
            placeholder='Title of news'
            error={formError.title}
            onChange={e => setForm({ ...form, title: e.target.value })}
          />
        </FieldBox>
        <FieldBox fluid>
          <FieldArea
            label='Short description (250)'
            value={form.short_description}
            placeholder='Tell us short description...'
            error={formError.short_description}
            onChange={e => setForm({ ...form, short_description: e.target.value })}
          />
        </FieldBox>
        <FieldBox fluid>
          <FieldArea
            label='Long description'
            value={form.long_description}
            placeholder='Tell us Long description...'
            onChange={e => setForm({ ...form, long_description: e.target.value })}
          />
        </FieldBox>
        <FieldBox>
          <Field
            label='Cover image address'
            value={form.cover_image}
            placeholder='Cover image of news'
            onChange={e => setForm({ ...form, cover_image: e.target.value })}
          />
        </FieldBox>
        <FieldBoxImage>
          <img
            src={form.cover_image}
            onError={e => { e.target.src = '' }}
          />
        </FieldBoxImage>
        <Divider className='divider-form' />
        <FieldBoxButton>
          <Button
            type='submit'
            color='green'
            loading={isLoading}
            disabled={isLoading}
            className='btn-form'
          >
            Confirm
          </Button>
          <Button
            type='button'
            loading={isLoading}
            disabled={isLoading}
            className='btn-form'
            onClick={() => {
              setForm(initForm)
              setFormError(initErrorForm)
            }}
          >
            Cancel
          </Button>
        </FieldBoxButton>
      </Form>
    </Wrapper>
  )
}

CreateEditNews.propTypes = {
  type: string,
  user: object,
  news: object,
}

export default CreateEditNews