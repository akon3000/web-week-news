import moment from 'moment'
import { getOffset } from '../utils/pagination'
import baseApiUrl from '../utils/baseApiUrl'
import requestApi from '../utils/requestApi'

export const list = (form = {}) => {
  const filters = []
  const { page = 1, limit = 20 } = form

  filters.push(`limit=${limit}`)
  filters.push(`offset=${getOffset(page, limit)}`)

  if (form.startDate) {
    filters.push(`start_date=${moment(form.startDate).format('YYYY-MM-DD')}`)
  }
  if (form.endDate) {
    filters.push(`end_date=${moment(form.endDate).format('YYYY-MM-DD')}`)
  }
  if (form.orderBy) {
    filters.push(`order_by=${form.orderBy}`)
  }

  return requestApi.get(`${baseApiUrl()}/news?${filters.join('&')}`)
}

export const detail = (id) => {
  return requestApi.get(`${baseApiUrl()}/news/${id}`)
}

export default {
  list,
  detail
}