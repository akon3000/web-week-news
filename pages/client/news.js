import { Component } from 'react'
import userService from '../../services/user'
import newsService from '../../services/news'
import NewsContainer from '../../containers/News'

class NewsPage extends Component {
  static async getInitialProps(ctx) {
    const userResult = await userService(ctx)
   
    if (userResult.error) {
      return {
        ...userResult
      }
    }

    try {
      const { id } = ctx.query
      const { data: response } = await newsService.detail(id)

      return {
        ...userResult,
        news: response.data
      }
    } catch (err) {
     
      return {
        error: err.response.status
      }
    }
  }

  render() {
    const { news } = this.props
    return <NewsContainer news={news} />
  }
}

export default NewsPage