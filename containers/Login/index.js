import { useState } from 'react'
import Router from 'next/router'
import styled from 'styled-components'
import { Divider, Button } from 'semantic-ui-react'
import { neutralColors } from '../../utils/css'
import { setCookie } from '../../utils/cookie'
import baseApiUrl from '../../utils/baseApiUrl'
import requestApi from '../../utils/requestApi'
import errorRequest from '../../utils/errorRequest'
import Field from '../../components/Field'

const LogoApp = styled('div')`
  font-size: 40px;
  font-family: 'Shadows Into Light', cursive;
  text-align: center;
  margin: 20px 0 50px 0;
  color: ${neutralColors.dark};
  font-weight: bold;
`

const FiledBox = styled('div')`
  margin-bottom: 20px;
`

const Wrapper = styled('div')`
  width: 100%;
  margin: auto;
  padding: 30px;
  max-width: 600px;
  border-radius: 10px;
  background-color: ${neutralColors.white};
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  .divider-login {
    color: ${neutralColors.solidGrey};
  }
`

const Login = () => {
  const [isLoading, setIsLoading] = useState(false)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [formError, setFormError] = useState({ email: null, password: null })

  const handleSubmit = async e => {
    e.preventDefault()
    
    if (email === '') {
      setFormError({ ...formError, email: 'This filed is required!' })
      return
    }
    if (password === '') {
      setFormError({ ...formError, password: 'This filed is required!' })
      return
    }

    setIsLoading(true)
    try {
      const { data: response } = await requestApi.post(`${baseApiUrl()}/login`, { email, password })

      if (response.status) {
        setCookie('authToken', response.data)
        Router.push('/')
      } else {
        alert(response.message)
      }

      setIsLoading(false)
    } catch (err) {
      setIsLoading(false)
      if (err.response) {
        errorRequest(err.response.status)
      }
    }

  }

  return (
    <Wrapper>
      <LogoApp>Week News</LogoApp>
      <Divider horizontal className='divider-login'>Login</Divider>
      <form onSubmit={handleSubmit}>
        <FiledBox>
          <Field
            label='Email'
            value={email}
            error={formError.email}
            placeholder='Please enter your email'
            onChange={e => {
              setFormError({ ...formError, email: null })
              setEmail(e.target.value)
            }}
          />
        </FiledBox>
        <FiledBox>
          <Field
            type='password'
            label='Password'
            value={password}
            error={formError.password}
            placeholder='Please enter your password'
            onChange={e => {
              setFormError({ ...formError, password: null })
              setPassword(e.target.value)
            }}
          />
        </FiledBox>
        <Button
          fluid
          size='large'
          type='submit'
          color='green'
          loading={isLoading}
        >
          Login
        </Button>
      </form>
    </Wrapper>
  )
}

export default Login