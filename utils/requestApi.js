import axios from 'axios'

const send = (method = 'GET', baseURL = '', data = {}, headers = {}) => axios({ method, baseURL, headers, data })

export const get = (url, headers) => send('GET', url, null, headers)

export const post = (url, body, headers) => send('POST', url, body, headers)

export const put = (url, body, headers) => send('PUT', url, body, headers)

export const del = (url, body, headers) => send('DELETE', url, body, headers)

export const withAuth = (token, type = 'bearer') => ({ authorization: `${type} ${token}` })

export default {
  get,
  post,
  put,
  del,
  withAuth
}