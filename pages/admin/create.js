import { Component } from 'react'
import redirect from '../../utils/redirect'
import userService from '../../services/user'
import CreateEditNewsContainer from '../../containers/AdminCreateEditNews'

class CreatePage extends Component {
  static async getInitialProps(ctx) {
    const userResult = await userService(ctx)

    if (userResult.user && userResult.user.isAdmin) {
      return {
        layout: {
          hasBodyAdmin: true
        },
        type: 'create',
        ...userResult
      }
    } else {
      /**
       * If user not found or user not have admin permission
       * redirect user to go to '/'
       */
      redirect('/', ctx) 
    }

  }

  render() {
    return <CreateEditNewsContainer {...this.props} />
  }
}

export default CreatePage