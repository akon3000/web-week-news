import { string, number } from 'prop-types'
import styled from 'styled-components'
import { screen, neutralColors } from '../../utils/css'
import { IMAGE_ERROR } from '../../constants/image'

const ContentRight = styled('div')`
  padding: 24px;
  display: inline-flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: calc(50% - 3px);

  @media ${screen.TABLET} {
    width: 80%;
  }
`

const ContentLeft = styled(ContentRight)`
  & > img {
    width: 100%;
    max-width: 350px;
  }
`

const ErrorCode = styled('p')`
  margin: 0;
  line-height: 1;
  font-size: 90px;
  font-weight: bold;
  text-align: center;
  font-style: normal;
  font-stretch: normal;
  letter-spacing: normal;
  color: ${neutralColors.dark};
  &::first-letter {
    text-transform: uppercase;
  }
`

const ErrorCodeTitle = styled(ErrorCode)`
  line-height: 1.2;
  font-size: 20px;
  margin-bottom: 15px;
  color: ${neutralColors.darkGrey};
`

const ErrorDescription = styled('p')`
  margin: 0;
  font-size: 16px;
  text-align: center;
  color: ${neutralColors.solidGrey};
  &::first-letter {
    text-transform: uppercase;
  }
`

const Wrapper = styled('div')`
  width: 100%;
  margin: auto;
  display: flex;
  padding: 40px 0;
  max-width: 850px;
  border-radius: 10px;
  background-color: ${neutralColors.white};
  box-shadow: 0 5px 10px 0 rgba(20, 30, 50, 0.1);

  @media ${screen.TABLET} {
    flex-direction: column;
    align-items: center;
    margin-bottom: 40px;
    width: calc(100% - 16px);
  }
`

const Error = ({ statusCode, codeTitle, title, message }) => (
  <Wrapper>
    <ContentLeft>
      <img src={IMAGE_ERROR} />
    </ContentLeft>
    <ContentRight>
      <ErrorCode>{statusCode || 'Oops!!'}</ErrorCode>
      <ErrorCodeTitle>{codeTitle}</ErrorCodeTitle>
      <ErrorDescription>{title}</ErrorDescription>
      <ErrorDescription>{message}</ErrorDescription>
    </ContentRight>
  </Wrapper>
)

Error.propTypes = {
  statusCode: number,
  codeTitle: string,
  title: string,
  message: string
}

export default Error