import Link from 'next/link'
import styled from 'styled-components'
import { object, func } from 'prop-types'
import { Icon } from 'semantic-ui-react'
import { screen, primaryColors, neutralColors } from '../../utils/css'
import DropDown from '../DropDown'

const Container = styled('div')`
  width: 100%;
  margin: auto;
  display: flex;
  height: 60px;
  justify-content: space-between;
  align-items: center;
  max-width: 1156px;
`
const NavContainer = styled('div')`
  display: inline-flex;
  align-items: center;
`

const NavBarLogo = styled('div')`
  a {
    font-size: 40px;
    font-family: 'Shadows Into Light', cursive;
    color: ${neutralColors.white};
    text-decoration: none;
  }

  @media ${screen.TABLET} {
    a {
      font-size: 25px;
    }
  }
`

const NavBarLogout = styled('div')`
  font-size: 20px;
  cursor: pointer;
  font-weight: 500;
  margin-left: 20px;
  color: ${neutralColors.white};

  @media ${screen.TABLET} {
    font-size: 16px;
  }

  @media ${screen.MOBILE_L} {
    display: none;
  }
`

const NavBarUser = styled('div')`
  max-width: 120px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  color: ${neutralColors.white};
  a {
    font-size: 20px;
    font-weight: 500;
    color: ${neutralColors.white};
    text-decoration: none;
  }

  @media ${screen.TABLET} {
    a {
      font-size: 16px;
    }
  }

  @media ${screen.MOBILE_L} {
    display: none;
  }
`

const NavBarConsole = styled(NavBarUser)`
  margin-left: 30px;

  @media ${screen.TABLET} {
    margin-left: 10px;
  }

  @media ${screen.MOBILE_L} {
    display: none;
  }
`

const NavBarMobileMenu = styled('div')`
  display: none;
  font-size: 20px;
  cursor: pointer;
  color: ${neutralColors.white};

  @media ${screen.MOBILE_L} {
    display: block;
  }
`

const Wrapper = styled('nav')`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  padding: 0 20px;
  user-select: none;
  z-index: 9999;
  background-color: ${primaryColors.green};
`

// End styled-components -------------------

const Header = ({ user, onSignOut }) => {

  const mobileMenu = []

  if (user && user.isAdmin) {
    mobileMenu.push(
      <Link href='/admin/home' as='/admin'>
        <a><Icon name='pencil' />Console</a>
      </Link>
    )
  }

  mobileMenu.push(
    user ? (
      <a onClick={onSignOut}>
        <Icon name='sign-out' /> Sign out
      </a>
    ) : (
      <Link href='/client/login' as='/login'>
        <a><Icon name='user circle' />Sign in</a>
      </Link>
    )
  )

  return (
    <Wrapper>
      <Container>
        <NavContainer>
          <NavBarLogo>
            <Link href='/client/home' as='/'>
              <a>Week News</a>
            </Link>
          </NavBarLogo>
          {user && user.isAdmin &&
            <NavBarConsole>
              <Link href='/admin/home' as='/admin'>
                <a><Icon name='pencil' />Console</a>
              </Link>
            </NavBarConsole>
          }
        </NavContainer>
        <NavContainer>
          {user &&
            <>
              <NavBarUser title={user.name}>
                <a><Icon name='user circle' /> {user.name}</a>
              </NavBarUser>
              <NavBarLogout title='sign out' onClick={onSignOut}>
                <Icon name='sign-out' /> 
              </NavBarLogout>
            </>
          }
          {!user &&
            <NavBarUser>
              <Link href='/client/login' as='/login'>
                <a><Icon name='user circle' />Sign in</a>
              </Link>
            </NavBarUser>
          }
          <NavBarMobileMenu>
            <DropDown
              iconMenu='list ul'
              direction='left'
              items={mobileMenu}
            />
          </NavBarMobileMenu>
        </NavContainer>
      </Container>
    </Wrapper>
  )
}

Header.propTypes = {
  user: object,
  onSignOut: func
}

export default Header