import { string, bool } from 'prop-types'
import { Input } from 'semantic-ui-react'
import styled, { css } from 'styled-components'
import { neutralColors, utilityColors } from '../../utils/css'

const LabelField = styled('label')`
  display: block;
  margin-bottom: 10px;
  color: ${neutralColors.solidGrey};
`

const LabelError = styled('label')`
  display: block;
  margin: 10px 0;
  color: ${utilityColors.danger};
`

const Wrapper = styled('div')`
  ${({ hasError }) => hasError && css`
    .input-field {
      & > input {
        border: solid 1px ${utilityColors.danger} !important;
        &:focus {
          border: solid 1px ${utilityColors.danger} !important;
        }
      }
    }
  `}
`

const Field = ({
  label,
  fluid,
  error,
  className,
  ...rest
}) => (
  <Wrapper hasError={error}>
    {label && <LabelField>{label}</LabelField>}
    <Input
      fluid={fluid}
      className={`input-field ${className}`}
      {...rest}
    />
    {error && <LabelError>{error}</LabelError>}
  </Wrapper>
)

Field.propTypes = {
  label: string,
  fluid: bool,
  error: string,
  className: string
}

Field.defaultProps = {
  fluid: true,
  className: ''
}

export default Field