if (!process.env.NODE_ENV) { // support for running on nodejs
  require('dotenv').config({ path: '.env' })
}

module.exports = {
  useFileSystemPublicRoutes: false,
  webpackDevMiddleware: (config) => {
    // Solve compiling problem via vagrant
    config.watchOptions = {
      poll: 1000, // Check for changes every second
      aggregateTimeout: 300, // delay before rebuilding
    }
    return config
  },
  publicRuntimeConfig: {
    API_CLIENT_URL: process.env.API_CLIENT_URL,
    API_SERVER_URL: process.env.API_SERVER_URL
  }
}