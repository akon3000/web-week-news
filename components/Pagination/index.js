import styled from 'styled-components'
import { string, number, func, bool } from 'prop-types'
import { Button } from 'semantic-ui-react'
import { neutralColors } from '../../utils/css'
import { getPage, getTotalPage } from '../../utils/pagination'

const Wrapper = styled('div')`
  display: inline-block;

  .ui.button {
    margin: 0 8px 0 0;
  }
`

const Pagination = ({
  step,
  size,
  color,
  onClick,
  disabled,
  totalItems,
  currentPage,
  itemPerPage
}) =>  {
  const totalPage = getTotalPage(totalItems, itemPerPage)
  const pages = getPage(currentPage, totalPage, step)

  return (
    <Wrapper>
      <Button
        basic
        size={size}
        color={color}
        icon='chevron left'
        disabled={disabled || currentPage === 1}
        onClick={e => {
          const prevPage = currentPage - 1
          if (prevPage < 1 || !onClick) {
            e.preventDefault()
          } else {
            onClick(prevPage)
          }
        }}
      />
      {
        pages.map((page, index) => (
          <Button
            key={index}
            size={size}
            color={color}
            disabled={disabled}
            basic={page !== currentPage}
            active={page === currentPage}
            onClick={e => {
              if (page === '...' || !onClick) {
                e.preventDefault()
              } else {
                onClick(page)
              }
            }}
          >
            {page}
          </Button>
        ))
      }
      <Button
        basic
        size={size}
        color={color}
        icon='chevron right'
        disabled={disabled || currentPage === totalPage}
        onClick={e => {
          const nextPage = currentPage + 1
          if (nextPage > totalPage || !onClick) {
            e.preventDefault()
          } else {
            onClick(nextPage)
          }
        }}
      />
    </Wrapper>
  )
}

Pagination.propTypes = {
  step: number,
  size: string,
  onClick: func,
  color: string,
  disabled: bool,
  totalItems: number,
  currentPage: number,
  itemPerPage: number
}

Pagination.defaultProps = {
  step: 2,
  size: 'mini',
  color: 'blue',
  totalItems: 0,
  currentPage: 1,
  itemPerPage: 20,
  disabled: false,
}

export default Pagination