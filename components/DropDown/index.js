import { string, arrayOf, node } from 'prop-types'
import { Dropdown } from 'semantic-ui-react'

const DropDown = ({
  items,
  iconMenu,
  direction
}) => (
  <Dropdown
    icon={iconMenu}
    direction={direction}
  >
    <Dropdown.Menu>
      {
        items.map((item, index) => (
          <Dropdown.Item key={index}>{item}</Dropdown.Item>
        ))
      }
    </Dropdown.Menu>
  </Dropdown>
)

DropDown.propTypes = {
  iconMenu: string,
  direction: string,
  items: arrayOf(node)
}

DropDown.defaultProps = {
  iconMenu: 'list ul',
  direction: 'right',
  items: []
}

export default DropDown