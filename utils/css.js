import * as size from '../constants/breakPoint'
import * as colors from '../constants/colors'

export const screen = {
  MOBILE_S: `(max-width: ${size.MOBILE_S})`,
  MOBILE_M: `(max-width: ${size.MOBILE_M})`,
  MOBILE_L: `(max-width: ${size.MOBILE_L})`,
  TABLET: `(max-width: ${size.TABLET})`,
  LAPTOP: `(max-width: ${size.LAPTOP})`,
  LAPTOP_L: `(max-width: ${size.LAPTOP_L})`,
  DESKTOP: `(max-width: ${size.DESKTOP})`,
  DESKTOP_L: `(max-width: ${size.DESKTOP})`,
}

export const primaryColors = {
  green: colors.GREEN,
  blue: colors.BLUE,
  gradientLeft: colors.GRADIENT_LEFT,
  gradientRight: colors.GRADIENT_RIGHT
}

export const neutralColors = {
  dark: colors.DARK,
  white: colors.WHITE,
  darkGrey: colors.DARK_GREY,
  solidGrey: colors.SOLID_GREY,
  lightGrey: colors.LIGHT_GREY,
  background: colors.BACKGROUND,
  disableGrey: colors.DISABLE_GREY
}

export const utilityColors = {
  danger: colors.DANGER,
  warning: colors.WARNING,
  success: colors.SUCCESS
}

export default {
  screen,
  primaryColors,
  neutralColors,
  utilityColors
}