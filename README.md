# web-week-news
---

This project is example WEB application for interview with company **Bitazza**.
It have communicate data also with WEB API.
Before running, you should download and install [(api-week-news)](https://gitlab.com/akon3000/api-week-news) first.

# 3rd framework usage
---

- [docker](https://www.docker.com/)
- [node js](https://nodejs.org/en/)
- [next js](https://nextjs.org/)
- [react](https://reactjs.org/)
- [express](https://expressjs.com/)
- [axios](https://github.com/axios/axios), for http request
- [semantic-ui-react](https://react.semantic-ui.com/)
- [react-datepicker](https://reactdatepicker.com/)

# Installation
---

Clone repository to your computer.

```
git clone git@gitlab.com/akon3000/web-week-news.git
```

# Start project
---

Go to web-week-news directory by terminal.

```
cd web-week-news
```

If you want to run project by NodeJS.

```
- npm install
- npm run build
```

After you run `build` command. you can use below this command for running application.

```
npm run dev
```

However, if you don't want `dev` environment. you should change variable `NODE_ENV` to `production` in `.env` file.
and run this command

```
npm run start
```

---

But you can run this project by Docker compose

```
- docker-compose up --build
```

# Admin console
---

Any console page for admin need Authentication. Please use this account for access the page.
- account `admin@weeknews.com`
- password `weeknews`