import { Component } from 'react'
import redirect from '../../utils/redirect'
import userService from '../../services/user'
import newsService from '../../services/news'
import CreateEditNewsContainer from '../../containers/AdminCreateEditNews'

class EditPage extends Component {
  static async getInitialProps(ctx) {
    const userResult = await userService(ctx)

    if (userResult.user && userResult.user.isAdmin) {
      
      const { id } = ctx.query

      if (!id) { // not found news
        return {
          error: 404
        }
      }

      try {
        const { data: response } = await newsService.detail(id)

        return {
          layout: {
            hasBodyAdmin: true
          },
          type: 'edit',
          news: response.data,
          ...userResult
        }

      } catch (err) {
        return {
          error: err.response.status
        }
      }

    } else {
      /**
       * If user not found or user not have admin permission
       * redirect user to go to '/'
       */
      redirect('/', ctx) 
    }

  }

  render() {
    return <CreateEditNewsContainer {...this.props} />
  }
}

export default EditPage