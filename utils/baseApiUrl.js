import getConfig from 'next/config'

const {
  publicRuntimeConfig: {
    API_CLIENT_URL,
    API_SERVER_URL
  }
} = getConfig()

export default () => (typeof window === 'undefined') ? API_SERVER_URL : API_CLIENT_URL