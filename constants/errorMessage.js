export const ERROR_WITHOUT_STATUS = {
  codeTitle: "Unknown",
  title: "An error occurred on the server",
  message: "Please try agin."
}

export const ERROR_WITH_STATUS_401 = {
  codeTitle: "Unauthorized",
  title: "Sorry, we can\'\t authorized that page!",
  message: "Authentication was provided, but the authenticated user is not permitted to perform the requested operation."
}

export const ERROR_WITH_STATUS_403 = {
  codeTitle: "Forbidden",
  title: "Sorry, you not have permission",
  message: "Authentication was provided, but the authenticated user is not permitted to perform the requested operation."
}

export const ERROR_WITH_STATUS_404 = {
  codeTitle: "Page not found",
  title: "Sorry, we can't find that page!",
  message: "Either something went wrong or the page doesn't exist anymore."
}

export const ERROR_WITH_STATUS_500 = {
  codeTitle: "Internal Server Error",
  title: "Sorry, something went wrong",
  message: "It looks as though we've broken something on our system"
}