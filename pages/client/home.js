import { Component } from 'react'
import userService from '../../services/user'
import HomeContainer from '../../containers/Home'

class HomePage extends Component {
  static async getInitialProps(ctx) {
    const userResult = await userService(ctx)
    const { limit } = ctx.query

    return {
      limit: parseInt(limit),
      ...userResult,
    }    
  }

  render() {
    const { limit } = this.props

    return <HomeContainer limit={limit ? limit : 12} />
  }
}

export default HomePage