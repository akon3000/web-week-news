import Router from 'next/router'
import { node, bool } from 'prop-types'
import { createGlobalStyle } from 'styled-components'
import { neutralColors, primaryColors } from '../../utils/css'
import { removeCookie } from '../../utils/cookie'
import Header from './Header'
import Body from './Body'

const GlobalCss = createGlobalStyle`
  html, body {
    margin: 0;
  }

  body {
    overflow: auto;
    font-size: 16px;
    font-family: 'Roboto', sans-serif;
    padding-top: ${({ paddingTop }) => paddingTop};
    background-color: ${neutralColors.background};
  }

  .ui.green.button {
    background-color: ${primaryColors.green};
    &:hover, &:active {
      background-color: #01A785;
    }
  }
  .ui.green.button.active {
    background-color: #01A785;
  }
`

const Layout = ({
  user,
  noHeader,
  hasBodyAdmin,
  children
}) => {

  const handleSignOut = () => {
    removeCookie('authToken')
    Router.push({ pathname: '/', asPath: '/client/home' })
  }

  return (
    <>
      <GlobalCss
        paddingTop={noHeader ? 0 : '60px'}
      />
      {!noHeader &&
        <Header
          user={user}
          onSignOut={handleSignOut}
        />
      }
      <Body hasBodyAdmin={hasBodyAdmin}>
        {children}
      </Body>
    </>
  )
}

Layout.propTypes = {
  children: node,
  noHeader: bool,
  hasBodyAdmin: bool
}

export default Layout