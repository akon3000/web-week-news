import { css } from 'styled-components'

export const GREEN = '#28D2AF'
export const BLUE = '#008cc8'
export const GRADIENT_LEFT = css`
  background: rgb(40, 210, 175);
  background: linear-gradient(
    90deg,
    rgba(40, 210, 175, 1) 0%,
    rgba(0, 140, 200, 1) 100%
  );
`
export const GRADIENT_RIGHT = css`
  background: rgb(40, 210, 175);
  background: linear-gradient(
    270deg,
    rgba(40, 210, 175, 1) 0%,
    rgba(0, 140, 200, 1) 100%
  );
`

export const DARK = '#141E32'
export const DARK_GREY = '#424A5A'
export const SOLID_GREY = '#788796'
export const LIGHT_GREY = '#E1E6F0'
export const DISABLE_GREY = '#BABDC6'
export const BACKGROUND = '#EFF2F7'
export const WHITE = '#FFFFFF'
export const BLACK = '#000000'

export const DANGER = '#FF7285'
export const WARNING = '#FFB900'
export const SUCCESS = '#4AD991'