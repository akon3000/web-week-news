import Link from 'next/link'
import styled from 'styled-components'
import { List } from 'semantic-ui-react'
import { screen, neutralColors } from '../../utils/css'

const ContentLeft = styled('div')`
  width: 300px;
  padding: 0 10px;
  margin-bottom: 20px;

  @media ${screen.TABLET} {
    width: 100%;
    padding: 15px 0 0;
  }
`

const ContentRight = styled('div')`
  flex-grow: 1;
  padding: 0 10px;

  @media ${screen.TABLET} {
    width: 100%;
    padding: 0;
  }
`

const MenuBox = styled('div')`
  padding: 20px;
  position: sticky;
  top: 80px;
  background-color: ${neutralColors.white};
`

const Wrapper = styled('div')`
  display: flex;
  
  @media ${screen.TABLET} {
    flex-wrap: wrap;
  }
`

const Body = ({ children }) => (
  <Wrapper>
    <ContentLeft>
      <MenuBox>
        <List animated divided relaxed>
          <List.Item>
            <Link href='/admin/home' as='/admin'>
              <a><List.Icon name='table' /> Dashboard</a>
            </Link>
          </List.Item>
          <List.Item>
            <Link href='/admin/create' as='/admin/create'>
              <a><List.Icon name='pencil' /> Create news</a>
            </Link>
          </List.Item>
        </List>
      </MenuBox>
    </ContentLeft>
    <ContentRight>{children}</ContentRight>
  </Wrapper>
)

export default Body