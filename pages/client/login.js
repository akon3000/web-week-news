import { Component } from 'react'
import { getCookie } from '../../utils/cookie'
import redirect from '../../utils/redirect'
import LoginContainer from '../../containers/Login'

class LoginPage extends Component {
  static async getInitialProps(ctx) {
    const authToken = getCookie('authToken', ctx.req)

    if (authToken) {
      return redirect('/', ctx)
    }
    
    return {
      layout: {
        noHeader: true
      }
    }
  }

  render() {
    return <LoginContainer />
  }
}

export default LoginPage