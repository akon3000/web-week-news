import { Component } from 'react'
import * as errorMessage from '../constants/errorMessage'
import ErrorContainer from '../containers/Error'

const getErrorMessage = statusCode => {
  switch (statusCode) {
    case 401:
      return errorMessage.ERROR_WITH_STATUS_401
    case 403:
      return errorMessage.ERROR_WITH_STATUS_403
    case 404:
      return errorMessage.ERROR_WITH_STATUS_404
    case 500:
      return errorMessage.ERROR_WITH_STATUS_500
    default:
      return errorMessage.ERROR_WITHOUT_STATUS
  }
}
class AppError extends Component {
  static getInitialProps({ res, err, query }) {
    const statusCode = res ? res.statusCode : err ? err.statusCode : query.statusCode ? parseInt(query.statusCode) : null
    return {
      statusCode,
      layout: {
        noHeader: true
      }
    }
  }

  render() {
    const { statusCode } = this.props
    const errorMsg = getErrorMessage(statusCode)
    return (
      <ErrorContainer
        {...errorMsg}
        statusCode={statusCode}
      />
    )
  }
}

export default AppError