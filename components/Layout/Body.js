import styled from 'styled-components'
import { screen } from '../../utils/css'
import BodyAdmin from './BodyAdmin'

const Container = styled('div')`
  width: 100%;
  margin: auto;
  max-width: 1156px;
`

const Wrapper = styled('div')`
  padding: 20px;

  @media ${screen.MOBILE_L} {
    padding: 5px;
  }
`

const Body = ({ children, hasBodyAdmin }) => (
  <Wrapper>
    {hasBodyAdmin
      ? (
        <Container>
          <BodyAdmin>{children}</BodyAdmin>
        </Container>
      ) : (
        <Container>{children}</Container>
      )
    }
  </Wrapper>
)

export default Body