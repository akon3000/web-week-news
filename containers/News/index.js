import { object } from 'prop-types'
import styled from 'styled-components'
import { neutralColors } from '../../utils/css'
import Header from './Header'

const NewFeedDescription = styled('div')`
  padding: 30px;
`

const Wrapper = styled('div')`
  box-shadow: rgb(204, 204, 204) 1px 1px 5px 0;
  background-color: ${neutralColors.white};
`

const News = ({ news }) => (
  <Wrapper>
    <Header
      title={news.title}
      creator={news.create_by}
      imageUrl={news.cover_image}
      detail={news.short_description}
    />
    <NewFeedDescription>
      {news.long_description}
    </NewFeedDescription>
  </Wrapper>
)

News.propTypes = {
  news: object
}

export default News