export default (params) => {
  const filters = []
  
  Object.keys(params).forEach(key => {
    filters.push(`${key}=${params[key]}`)
  })

  return `?${filters.join('&')}`
}