/**
 * Helpers pagination
 * Logic from => https://codepen.io/anon/pen/yrqVZp
 */

const addPage = (start, end) => {
  const items = []
  for (let i = start; i < end; i += 1) {
    items.push(i)
  }
  return items
}

const addFirstPage = () => [1, '...']

const addLastPage = totalPage => ['...', totalPage]

export const getOffset = (page, limit) => (page < 0 ? 0 : (page - 1) * limit)

export const getTotalPage = (totalItems, itemPerPage) => Math.ceil(totalItems / itemPerPage)

export const getPage = (currentPage, totalPage, step) => {
  if (totalPage < (step * 2) + 6) {
    return addPage(1, totalPage + 1)
  }
  if (currentPage < (step * 2 + 1)) {
    return [
      ...addPage(1, (step * 2) + 4),
      ...addLastPage(totalPage),
    ]
  }
  if (currentPage > (totalPage - (step * 2))) {
    return [
      ...addFirstPage(),
      ...addPage((totalPage - (step * 2)) - 2, totalPage + 1),
    ]
  }
  return [
    ...addFirstPage(),
    ...addPage(currentPage - step, currentPage + (step + 1)),
    ...addLastPage(totalPage),
  ]
}

export default {
  getPage,
  getOffset,
  getTotalPage,
}
