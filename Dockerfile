# pull a node image from docker hub
FROM node:latest

# set the working dir to /app
WORKDIR /app

# copy everything to container
COPY . .

# install node package module
RUN npm install

# build application
RUN npm run build

# start server inside container
CMD ["npm", "run", "start"]
