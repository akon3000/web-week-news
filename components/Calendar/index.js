import DownShift from 'downshift'
import moment from 'moment'
import styled from 'styled-components'
import { string, instanceOf, func, object } from 'prop-types'
import Datepicker from  'react-datepicker'
import Field from '../Field'

const ContainerDropDown = styled('div')`
  position: absolute;
  left: 0;
  top: calc(100% + 5px);
  z-index: 1000;
`

const Wrapper = styled('div')`
  position: relative;

`

// End styled-components -----------

const stateReducer = (state, changes) => {
  switch (changes.type) {
  case DownShift.stateChangeTypes.blurInput:
    return { ...state, ...changes, isOpen: true }
  default:
    return { ...state, ...changes }
  }
}

const Calendar = ({
  value,
  maxDate,
  minDate,
  onChange,
  dateFormat,
  inputProps,
  idDownShift
}) => {
  const dateValue = value ? moment(value).format(dateFormat) : ''

  return (
    <DownShift
      id={idDownShift}
      stateReducer={stateReducer}
    >
      {({
        isOpen,
        openMenu,
        getRootProps,
        getInputProps
      }) => (
        <Wrapper {...getRootProps()}>
          <Field
            {...getInputProps({
              value: dateValue,
              onFocus: openMenu,
              ...inputProps
            })}
          />
          {isOpen
            ? (
              <ContainerDropDown>
                <Datepicker
                  inline
                  selected={value}
                  maxDate={maxDate}
                  minDate={minDate}
                  onChange={onChange}
                />
              </ContainerDropDown>
            ) : null
          }
        </Wrapper>
      )}
    </DownShift>
  )
}

Calendar.propTypes = {
  onChange: func,
  dateFormat: string,
  inputProps: object,
  idDownShift: string,
  value: instanceOf(Date),
  maxDate: instanceOf(Date),
  minDate: instanceOf(Date)
}

Calendar.defaultProps = {
  inputProps: {},
  dateFormat: 'D MMMM YYYY'
}

export default Calendar