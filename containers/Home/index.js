import { useState, useEffect } from 'react'
import Link from 'next/link'
import moment from 'moment'
import styled from 'styled-components'
import { number } from 'prop-types'
import { Card, Icon, Image, Loader, Button } from 'semantic-ui-react'
import newsService from '../../services/news'
import { screen, neutralColors } from '../../utils/css'
import Calendar from '../../components/Calendar'
import Pagination from '../../components/Pagination'

const FeedBox = styled('div')`
  width: 25%;
  padding: 10px;

  .ui.card {
    width: 100%;
  }

  .date {
    font-size: 12px;
    color: ${neutralColors.solidGrey};
  }

  .short-desc {
    font-size: 14px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    color: ${neutralColors.disableGrey};
  }

  @media ${screen.TABLET} {
    width: 50%;
  }

  @media ${screen.MOBILE_L} {
    width: 100%;
  }
`

const FeedContainer = styled('div')`
  display: flex;
  flex-wrap: wrap;
`

const PaginationContainer  = styled('div')`
  margin: 50px 0;
  text-align: center;
`

const CalendarContainer = styled(FeedContainer)`
  padding: 20px 20px 0;
  margin: 50px 10px;
  background-color: ${neutralColors.white};
  .start-date,
  .end-date {
    width: 250px;
    margin-bottom: 20px;
  }

  .end-date {
    margin-left: 20px;
  }
  .button-filter > button {
    margin-left: 20px;
    margin-bottom: 20px;
  }

  @media ${screen.TABLET} {
    .start-date,
    .end-date {
      width: 100%;
    }

    .end-date  {
      margin-left: 0;
    }

    .button-filter {
      width: 100%;
      text-align: right;
    }
  }
`

const Home = ({ limit }) => {
  const [isLoading, setIsLoading] = useState(true)
  const [currentPage, setCurrentPage] = useState(1)
  const [dateForm, setDateForm] = useState({ startDate: null, endDate: null })
  const [newsList, setNewsList] = useState([])
  const [newsListTotal, setNewsListTotal] = useState(0)

  const handleFetchNews = async () => {
    setIsLoading(true)
    try {
      const { data: response } = await newsService.list({ ...dateForm, page: currentPage, limit })
      
      if (response.status) {
        setNewsList(response.data)
        setNewsListTotal(response.max_count)
      } else {
        alert(response.message)
      }
      setIsLoading(false)      
    } catch (err) {
      setIsLoading(false)
      alert(err.response.message)
    }
  }

  useEffect(() => {
    handleFetchNews()
  }, [])

  return isLoading ? (
    <Loader
      active
      size='massive'
      inline='centered'
      style={{ marginTop: 100 }}
    >
      loading
    </Loader>
  ) : (
    <>
      <CalendarContainer>
        <div className='start-date'>
          <Calendar
            value={dateForm.startDate}
            maxDate={dateForm.endDate}
            inputProps={{ placeholder: 'Start date' }}
            onChange={date => setDateForm({ ...dateForm, startDate: date })}
          />
        </div>
        <div className='end-date'>
          <Calendar
            value={dateForm.endDate}
            minDate={dateForm.startDate}
            inputProps={{ placeholder: 'End date' }}
            onChange={date => setDateForm({ ...dateForm, endDate: date })}
          />
        </div>
        <div className='button-filter'>
          <Button
            circular
            size='large'
            color='green'
            icon='search'
            onClick={async () => {
              await setCurrentPage(1)
              await handleFetchNews()
            }}
          />
          <Button
            circular
            size='large'
            icon='cancel'
            onClick={() => setDateForm({ startDate: null, endDate: null })}
          />
        </div>
      </CalendarContainer>
      <FeedContainer>
        {
          newsList.map(news => (
            <FeedBox key={news.id}>
              <Card>
                <Image src={news.cover_image} wrapped />
                <Card.Content>
                  <Card.Header>{news.title}</Card.Header>
                  <Card.Meta>
                    <span className='date'>
                      Create in {moment(news.create_date).format('D MMM YYYY')}
                    </span>
                  </Card.Meta>
                  <Card.Description className='short-desc'>
                    {news.short_description}
                  </Card.Description>
                </Card.Content>
                <Card.Content extra textAlign='right'>
                  <Link href={`/client/news?id=${news.id}`} as={`/news/${news.id}`}>
                    <a>
                      Read more &nbsp;
                      <Icon name='arrow alternate circle right outline' />
                    </a>
                  </Link>
                </Card.Content>
              </Card>
            </FeedBox>
          ))
        }
      </FeedContainer>
      {newsList.length > 0 &&
        <PaginationContainer>
          <Pagination
            size='medium'
            itemPerPage={limit}
            currentPage={currentPage}
            totalItems={newsListTotal}
            onClick={async newPage => {
              if (newPage === currentPage) {
                return
              }
              await setCurrentPage(newPage)
              await handleFetchNews()
            }}
          />
        </PaginationContainer>
      }
    </>
  )
}

Home.propTypes = {
  limit: number
}

export default Home