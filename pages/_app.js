import NextApp from 'next/app'
import ErrorApp from './_error'
import Layout from '../components/Layout'

class App extends NextApp {
  static async getInitialProps({ Component, ctx }) {
    return {
      pageProps: (Component.getInitialProps ? await Component.getInitialProps(ctx) : {})
    }
  }

  render() {
    const {
      pageProps: {
        user,
        error,
        layout = {},
        ...pageProps
      },
      Component
    } = this.props

    if (error) {
      return <ErrorApp statusCode={error} />
    }

    return (
      <Layout
        user={user}
        noHeader={layout.noHeader}
        hasBodyAdmin={layout.hasBodyAdmin}
      >
        <Component
          {...pageProps}
          user={user}
        />
      </Layout>
    )
  }
}

export default App