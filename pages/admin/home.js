import { Component } from 'react'
import redirect from '../../utils/redirect'
import userService from '../../services/user'
import HomeContainer from '../../containers/AdminHome'

class HomePage extends Component {
  static async getInitialProps(ctx) {
    const userResult = await userService(ctx)

    if (userResult.user && userResult.user.isAdmin) {
      return {
        layout: {
          hasBodyAdmin: true
        },
        ...userResult
      }
    } else {
      /**
       * If user not found or user not have admin permission
       * redirect user to go to '/'
       */
      redirect('/', ctx) 
    }

  }

  render() {
    return <HomeContainer />
  }
}

export default HomePage