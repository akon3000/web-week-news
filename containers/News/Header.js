import { string } from 'prop-types'
import styled from 'styled-components'
import { screen, neutralColors } from '../../utils/css'
import { IMAGE_BLUR } from '../../constants/image'

const SvgBurl = styled('svg')`
  width: 100%;
  height: 100%;
`

const ContentBackground = styled('div')`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1;
`

const CoverImage = styled('div')`
  width: 200px;
  height: 280px;
  min-width: 200px;
  background-image: url(${({ image }) => image});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;
`

const NewFeedDetail = styled('div')`
  margin-left: 30px;

  h2 {
    margin-bottom: 5px;
  }

  .news-by {
    font-size: 12px;
    font-weight: 500;
    margin-bottom: 30px;
  }

  .detail {
    font-weight: 300;
  }

  @media ${screen.TABLET} {
    margin-left: 0;
    margin-top: 30px;

    h2, .news-by {
      text-align: center;
    }
  }
`

const ContentDetail = styled('div')`
  z-index: 2;
  display: flex;
  position: relative;

  @media ${screen.TABLET} {
    flex-wrap: wrap;
    justify-content: center;
  }
`

const Wrapper = styled('div')`
  padding: 30px;
  position: relative;
  color: ${neutralColors.white};
  background-color: ${neutralColors.solidGrey};
`

const Header = ({
  title,
  detail,
  creator,
  imageUrl,
}) => (
  <Wrapper>
    
    <ContentDetail>
      <CoverImage image={imageUrl} />
      <NewFeedDetail>
        <h2>{title}</h2>
        <div className='news-by'>By {creator}</div>
        <div className='detail'>{detail}</div>
      </NewFeedDetail>
    </ContentDetail>
    <ContentBackground>
      <SvgBurl
        viewBox='0 0 1 1.414'
        preserveAspectRatio='xMidYMid slice'
      >
        <defs>
          <filter
            x='0'
            y='0'
            width='100%'
            height='100%'
            id='blurFilter'
          >
            <feGaussianBlur
              stdDeviation='0.05'
            />
          </filter>
        </defs>
        <g>
          <image
            width='100%'
            height='100%'
            href={IMAGE_BLUR}
            filter='url(#blurFilter)'
          />
          <rect
            x='0'
            y='0'
            width='100%'
            height='100%'
            fillOpacity='0.58'
            fill={neutralColors.solidGrey}
            />
        </g>
      </SvgBurl>
    </ContentBackground>
  </Wrapper>
)

Header.propTypes = {
  title: string,
  detail: string,
  creator: string,
  imageUrl: string
}

export default Header