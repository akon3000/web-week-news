import jsWebCookie from 'js-cookie'
import jsHttpCookie from 'cookie'

export const setCookie = (key, value, options = { expires: 1 }) => {
  jsWebCookie.set(key, value, options)
}

export const removeCookie = (key, res) => {
  if (res) {
    res.clearCookie(key)
  } else {
    jsWebCookie.remove(key)
  }
}

export const getCookie = (key, req) => {
  return !req ? getCookieFromBrowser(key) : getCookieFromServer(key, req)
}

const getCookieFromBrowser = (key) => jsWebCookie.get(key)

const getCookieFromServer = (key, req) => {
  if (!req.headers.cookie) return undefined
  const cookiesJSON = jsHttpCookie.parse(req.headers.cookie)
  return cookiesJSON[key] ? cookiesJSON[key] : undefined
}

export default {
  getCookie,
  setCookie,
  removeCookie
}