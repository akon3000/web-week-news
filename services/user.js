import { getCookie, removeCookie } from '../utils/cookie'
import baseApiUrl from '../utils/baseApiUrl'
import requestApi, { withAuth } from '../utils/requestApi'

export default async (ctx = {}) => {
  const token = getCookie('authToken', ctx.req)

  if (!token) {
    return {}
  }

  try {
    const { data: resMe } = await requestApi.get(`${baseApiUrl()}/me`, withAuth(token))
      
    if (resMe.status) {
      const { data: resAdmin } = await requestApi.get(`${baseApiUrl()}/me/admin`, withAuth(token))

      return {
        user: { ...resMe.data, isAdmin: resAdmin.status }
      }
    } else {
      removeCookie('authToken', ctx.res) // remove because token not supported.
      return {}
    }
  } catch (err) {
    
    const { status } = err.response

    if (status === 415) {
      removeCookie('authToken', ctx.res) // remove because token is expired.
      return {}
    }

    return {
      error: status
    }
  }
}