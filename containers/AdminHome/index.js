import { useState, useEffect } from 'react'
import Link from 'next/link'
import styled from 'styled-components'
import { Divider, Loader, Table, Button, Grid } from 'semantic-ui-react'
import { neutralColors } from '../../utils/css'
import { getCookie } from '../../utils/cookie'
import baseApiUrl from '../../utils/baseApiUrl'
import requestApi, { withAuth } from '../../utils/requestApi'
import errorRequest from '../../utils/errorRequest'
import newsService from '../../services/news'
import Calendar from '../../components/Calendar'
import Pagination from '../../components/Pagination'

const CellImage = styled('div')`
  width: 200px;
  margin: auto;
  & > img {
    width: 100%;
    height: auto;
    display: block;
  }
`

const NewsListContent = styled('div')`
  overflow: auto;
  margin: 50px 0 0;
`

const PaginationContent = styled('div')`
  margin: 30px 0 0;
  text-align: right;
`

const Wrapper = styled('div')`
  padding: 20px;
  background-color: ${neutralColors.white};
`

const Home = () => {
  const [isLoading, setIsLoading] = useState(true)
  const [newsList, setNewsList] = useState([])
  const [newsListTotal, setNewsListTotal] = useState(0)
  const [formNews, setFormNews] = useState({
    page: 1,
    limit: 10,
    startDate: null,
    endDate: null
  })

  const handleFetchNews = async () => {
    setIsLoading(true)
    try {
      const { data: response } = await newsService.list(formNews)
      
      if (response.status) {
        setNewsList(response.data)
        setNewsListTotal(response.max_count)
      } else {
        alert(response.message)
      }
      setIsLoading(false)      
    } catch (err) {
      setIsLoading(false)
      if (err.response) {
        errorRequest(err.response.status)
      }
    }
  }

  const handleDeleteNews = async id => {
    if (confirm('Are you sure you want to delete this news ?')) {
      setIsLoading(true)
      try {
        const token = getCookie('authToken')
  
        const { data: response } = await requestApi.del(`${baseApiUrl()}/news/delete/${id}`, {}, withAuth(token))
        setIsLoading(false)
  
        alert(response.message)
  
        await handleFetchNews()
  
      } catch (err) {
        setIsLoading(false)
        if (err.response) {
          errorRequest(err.response.status)
        }
      }
    }
  }

  useEffect(() => {
    handleFetchNews()
  }, [formNews]) // watch state change and auto fetch news.

  return (
    <Wrapper>
      <h2>Dashboard console</h2>
      <Divider />
      <Grid columns={2}>
        <Grid.Row>
          <Grid.Column>
            <Calendar
              idDownShift='start-date'
              value={formNews.startDate}
              maxDate={formNews.endDate}
              onChange={date => setFormNews({ ...formNews, page: 1, startDate: date })}
              inputProps={{
                label: 'Start date',
                placeholder: 'Select date'
              }}
            />
          </Grid.Column>
          <Grid.Column>
            <Calendar
              idDownShift='end-date'
              value={formNews.endDate}
              minDate={formNews.startDate}
              onChange={date => setFormNews({ ...formNews, page: 1, endDate: date })}
              inputProps={{
                label: 'End date',
                placeholder: 'Select date'
              }}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      
      <NewsListContent>
        {isLoading
          ? (
            <Loader
              active
              size='massive'
              inline='centered'
            >
              loading
            </Loader>
          ) : (
            <Table
              celled
              striped
              unstackable
              textAlign='center'
            >
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell width={1}>No.</Table.HeaderCell>
                  <Table.HeaderCell width={4}>Image</Table.HeaderCell>
                  <Table.HeaderCell width={8}>Title</Table.HeaderCell>
                  <Table.HeaderCell colSpan='2' />
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {
                  newsList.map((news, index) => (
                    <Table.Row key={news.id}>
                      <Table.Cell>{(index + 1) * formNews.page}</Table.Cell>
                      <Table.Cell>
                        <CellImage>
                          <img src={news.cover_image} />
                        </CellImage>
                      </Table.Cell>
                      <Table.Cell>{news.title}</Table.Cell>
                      <Table.Cell>
                        <Link href={`/admin/edit?id=${news.id}`} as={`/admin/edit/${news.id}`}>
                          <a><Button icon='pencil' title='Edit' color='blue' /></a>
                        </Link>
                      </Table.Cell>
                      <Table.Cell>
                        <Button icon='trash' title='Delete' color='red' onClick={() => handleDeleteNews(news.id)} />
                      </Table.Cell>
                    </Table.Row>
                  ))
                }
              </Table.Body>
            </Table>
          )
        }
      </NewsListContent>
      {newsList.length > 0 &&
        <PaginationContent>
          <Pagination
            totalItems={newsListTotal}
            currentPage={formNews.page}
            itemPerPage={formNews.limit}
            onClick={page => setFormNews({ ...formNews, page })}
          />
        </PaginationContent>
      }
    </Wrapper>
  )
}

export default Home